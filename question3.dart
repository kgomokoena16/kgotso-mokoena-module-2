void main() {
  Winner winner = new Winner('shyft', 'banking', 'fnb', '2012');

  winner.printOutput();
  winner.transformString();
}

class Winner {
  var appName;
  var category;
  var developer;
  var year;

  //Constructor
  Winner(var appname, String category, String developer, String year) {
    this.appName = appname;
    this.category = category;
    this.developer = developer;
    this.year = year;
  }

  printOutput() {
    print(
        'In $year, $appName developed by $developer in the $category sector won MTN APP OF THE YEAR');
  }

  //Change appName to uppercase
  transformString() {
    print('In $year, ' +
        appName.toString().toUpperCase() +
        ' developed by $developer in the $category sector won MTN APP OF THE YEAR');
  }
}
