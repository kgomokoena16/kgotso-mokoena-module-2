void main() {
  //List of MTN App Of The Year overall winners from 2012 to 2021, sorted by year respectively
  List<String> mtnAppWinners = [
    'FNB',
    'SnapScan',
    'LIVE Inspect',
    'Wumdrop',
    'Domestly',
    'Shyft',
    'Khula',
    'Naked Insurance',
    'EasyEquities',
    'Ambani Africa'
  ];

  int totalApps = mtnAppWinners.length;

  //Sort the strings in the array alphabetically
  mtnAppWinners.sort();

  print('Winners since 2012 sorted by app name: $mtnAppWinners');

  print('The overall winner for 2017 was ' + mtnAppWinners[7]);

  print('The overall winner for 2018 was ' + mtnAppWinners[4]);

  print('The total number of apps that won so far is $totalApps');
}
